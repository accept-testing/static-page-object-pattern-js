import { TextInput } from '../../elements'

export const DataTable = {
    rows: 'tbody > tr',
    cells: '<td />',
    fields: () => {
        return {
            title: String,
            firstname: String,
            lastname: String,
            input: TextInput
        }
    }
}     

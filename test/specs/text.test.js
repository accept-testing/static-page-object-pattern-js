let should = require('chai').should;
let expect = require('chai').expect;
let webdriverio = require('webdriverio');
let path = require('path');

import 'babel-polyfill';
import { Text, Table } from '../../elements';
import { DataTable } from './data-table';

describe('Text', function() {    

    this.beforeEach(async function() {
        let htmlFile = 'file://' + path.join(__dirname, '..', '..' ,'html', 'table.html');
        await browser.url(htmlFile);
    });

    // it('should set text', async () => {        
    //     let text = new Text({
    //         locator: '[name=input_name]'
    //     })        
    //     text.setText('Enter some text')
    //     expect(text.getText()).to.equal('Enter some text')         
    // })   

    it('Get Data Table', () => {        
        
        let table = new Table({
            locator: '.test-table',
            model: DataTable
        })
        let data = table.getData();
        console.log(data)
    })

});
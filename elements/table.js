import Element from './element'
import Text from './text'

export default class Table extends Element {

    constructor(props) {
        super(props)
    }

    getData() {
        return this.rows()
            .map(row => this.cells(row))
            .map(cells => this.toModel(cells));
    }

    rows() { 
        return this.element.$$(this.props.model.rows);
    }

    cells(row) {
        return row.$$(this.props.model.cells);
    }

    toModel(cells) {

        let webElements = cells.values();
        let fields  = this.props.model.fields();
        
        if (Object.entries(cells).length !== Object.entries(fields).length) {
            throw new Error("The number of entries do not match the number of fields on the model.");
        }

        for (let [key, type] of Object.entries(fields)) {
            switch (type) {
                case String:
                    fields[key] = new Element( { element: webElements.next().value } ).getText();
                    break;
                case Text:
                    fields[key] = new Text( { element: webElements.next().value } );
                    break;
                default:
                    throw new Error( "Unsupported element type: " + type );
            }
        }

        return fields;
    }
}
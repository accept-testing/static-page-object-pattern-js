export default class Element {
    
    constructor(props) {
        this.props = props;
    }

    get element() {     
        if (this.props.locator === undefined) {
            
            console.log('***********')
            console.log(this.props.element.getText())
            console.log('***********')
            
            return this.props.element;
        }
        return $(this.props.locator);      
    }

    getText() {
        return (this.element.getText() === '') ? this.element.getAttribute('value') : this.element.getText();        
    }
        
}
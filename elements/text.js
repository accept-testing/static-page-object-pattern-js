import Element from './element'

export default class TextInput extends Element {

    constructor(props) {
        super(props)
    }

    setText(value) {
        this.element.setValue(value)
    }
    
}
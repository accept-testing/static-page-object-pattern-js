# Static Page Object Pattern JS

# Static Page Object Pattern (a 3 tiered pattern for web testing)

The Static Page Object design pattern is built around a primary goal of pages are only representable by a list of objects which exist on the page. Pages hold no further logic or page level interactions. Interaction with the webpage should only be interactable via page objects and never directly accessing the web recognition library. This has the effect of building an impenetrable layer between the page and low-level browser interaction.

The word **Static** defines the ability to call a Page and any of its objects at any time, without being bound to state.

As the static design is no longer interested in page level state, we delegate state to elements and therefore all elements require state level synchronisation.

Finally data marshalling is about providing page level objects, which represent list of repetitive data structures.

The paramount goal of the Static-Page-Object design is to a) cross typed; works with Java or Javascript b) data marshalling and element level synchronisation can live outside of this pattern and equally can be disregarded.

## Tier 1 -> Static Pages or Dumb Pages

An idiviual element should be represented by the following structure:

`Page.Object.method`

A sudo coded example of a TextInput on a Student enrolment form:

```Java
class EnrolmentPage {

    TextInput userName = new TextInput(CSS, ".user_name");

}
```

The result of the design is that from anywhere in your're test code the userName can be called as static object.

`EnrolmentPage.username.setText("Pablo")`

### Using the Static Page Objects

The **TextInput** object should contain all of the method required to interact with the page. e.g. setText / getText, these are essentially getters and setters.

For example the username setter could also be written like:

```Java
class User {
 
    public void setUserDetails() {
        EnrolmentPage.name.setText("Pablo");
        EnrolmentPage.lastname.setText("Smith");
    }
 
    public void getUser() {
        return EnrolmentPage.name.getText()
    }

}
```

## Tier 2 -> Element level synchronisation

Within the pre-text of PageObject pattern, state was controlled at the page level. Pages are returned back to the user with ready state. This design was aimed at a pre-ajax age wher single state pages were returned. With ajax and libraries such as react/angular this has made page level state more complex.

Due to the static nature of calling test objects, state must now be at the element level. Therefore all elements, such as TextInput will require a mechansim, such as BaseObject, which can leverage the ability to handle state. 

For example, when we call `EnrolmentPage.name.setText("Pablo");` we must make sure that `name` is ready to interact with.

In this JS example, we will provide examples of state within the object base classes.

As state managemwnt is key to the ability of each element to always be in a ready state. Implemenrarions such as JS can use state linearies such as Redux. This library or its principles are to create an wvent, which gets dispatched and using hooks or lifecycle methods will then wait for the event to return. This approach dits nicely into objwxt state sychonisation.

### Base Element Object

The BaseElement should:
 * All inherited element code should reside
 * All ajax calls should finalise, such as react/angular/jquery
 * Any bespoke handling of ajax calls should finalise
 * All initial element interaction
 * The only place webdriver/browser recognition should in the first instance be called

### Boxing In webdriver/browser library

In order to maintain separation between static page object the browser recongition library sparingly called, by using BaseElement indirectly or directly as the only place to call the browser recongition library, then state will be maintained. 

By calling browser recongition tool, independently there is a risk of failing to synchonise to the browser, giving arise to stale element exceptions.


## Tier 3 -> Data marshalling repetive object structures

Repetive structures, such as lists of objects, tables, or any structure, which represents a piece of repeated data can be marshalled into a new more accesible, more interactable object.

By building a component, or new class steucture we can provide this object with the instructions to map the on screen structure into the new component.

An example of table is the most simplest.

Student List:


| name | lastname | course |
| ------ | ------ | ----- |
| Lauren| McManus | Maths |
| Bryan | Wong | Maths |

After marshalling, we should create an object like.

Students
 - Student name: Lauren, lastname: McManus, course: Maths
 - Student name: Bryan, lastname: Wong, course: Maths
 
It is far easier to filter, find etc the data in the Students object, instead of specific css/xpath selector. 

This example, is simplified as all of the data can be mapped into an onject of strings. 

For a slighty more complicated object:

| lastname | course | enrolment |
| ------ | ------ | ----- |
| Smith| Art | [x] |
| Patel | Geography | [ ] |

The enrolment column, is supped to signify a checkbox. This remains a checkbox as a component and therefore all
of the methods developed for this still apply. 

Therefore if you are filtering or finding you can call the methods on this chechbox object.

e.g.

Students.find(student => student.lastname === "Patel").enrolled.setStatus(true);

This is achieved by providing to the Conponent 3 pieces of information.

1. The CSS/Selector for the column
2. The CSS/Selector for the row
3. The model or list of objects the row will map to

The Student object above could be in theory;

 Student
 
  const data = {
       fields = {
          name: String,
          lastname: String,
          course: String,
          enrolled: Checkbox,
        },
   row: "#tbody > tr",
   col: "thead > td"
  }
   
This component acts as a model, which upon marshallimg xontains all if the information to gwnerate a list of Student objects.

A further more complicated model, should allow a model to also appear inside anotuee model.

e.g.

  ModelParent
   fields = {
       element1: String,
       element2: ModelChild
   }

The model should always be initated in the same way any onject is initiated the same way any otjer static model is initiated by providimg the outer or parwnt element.

e.g. Students students = Student(Student, "#student-table")

## Running the test...

1. npm install
2. npm test



